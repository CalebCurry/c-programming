#ifndef LIST_H
#define LIST_H

#include <stdbool.h>

typedef struct list_t list_t;
typedef struct node_t node_t;

//dynamically allocates a list and returns the pointer
list_t *create_list ();

//deallocates all used memory (essentially this deletes the list)
void free_list (list_t *list);

//returns the number of elements in the list starting from 1 (not 0 based)
int list_size (list_t *list);

//takes a integer and creates a new node storing that integer, adding it to the end of the list.
void list_append (list_t *list, int item);

//TODO Does this function belong?
bool list_add (list_t *list, int item, int index);

//Removes a particular index from the array and relinks the elements.  Returns true if success.
bool list_remove (list_t *list, int index);

//Takes an integer value and returns the first index of the node containing that data.
int list_find (list_t *list, int item);

//Returns the data of a node at a particular index.  -1 if not found.
int list_get (list_t *list, int index);

//Grabs the node instance that has a particular index.  
node_t *get_node(list_t *list, int index);

//Return true of the index given is within range of linked list.
bool index_is_valid(list_t *list, int index);

#endif
