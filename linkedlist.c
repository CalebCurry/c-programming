#include <stdio.h>
#include <stdlib.h>
#include "linkedlist.h"

struct list_t
{
    struct node_t *first;
    struct node_t *last;
    int size;
};

struct node_t
{
    struct node_t *next;
    int data;
};

list_t *create_list ()
{
    list_t *list = malloc (sizeof(list_t));

    list->first = NULL;
    list->last = NULL;
    list->size = 0;

    return list;
}

void free_list (list_t *list)
{
	printf("The count of elements in the list is %d\n",list->size);
	node_t *node = list->first;
	node_t *temp = list->first;
	int i;
	for(i = 0; i < list->size; i++)	
	{
		//using the temp so once I free node I can still access node->next
		temp = node->next;
		free(node);
		node = temp;		
	}
	free(list);
}

int list_size (list_t *list)
{		
	return list->size;
}

void list_append (list_t *list, int item)
{
	node_t *new_node = malloc(sizeof(node_t));	
	new_node->data = item;

	//First element
	if(list->first == NULL)
	{
		list->first = new_node;
		list->last = new_node;
	} else //Not the first element...so add as last element
	{ 
		//first link from current last to new node that is being appended.
		list->last->next = new_node;
		//Now establish new node as the new last.
		list->last = new_node;	
	}
	list->size++;	
}

bool list_insert (list_t *list, int item, int index)
{
	
	/*The cases we have to consider are when the node is being inserted first or not first first*/
	if(!index_is_valid(list, index))
	{
		return false;
	}	
	
	node_t *new_node = malloc(sizeof(node_t));
	new_node->data = item;

	if(index == 0)
	{ //beginning
		//link new node to the current first node.
		new_node->next = list->first;		
		//set new node as the new first node.
		list->first = new_node;
	
	} else if((index > 0) && (index <= list->size-1))
	{ //middle or end
		node_t *previous_node = get_node(list, index-1);			
		new_node->next = previous_node->next; 
		previous_node->next = new_node;
	}
    	list->size++;
	return true;
}

bool list_remove (list_t *list, int index)
{		
	if(!index_is_valid(list, index))
	{
		return false;
	}
/*The cases we have to consider are when the node is first,last, or middle */
//Temporary variable used to deallocate memory:
node_t *removed_node;

	if(index == 0)
	{ //beginning
		removed_node = list->first;
		//link passsed the deleted node
		list->first = list->first->next;
			
	} else if(index == list->size-1)
	{ //end
		node_t *previous_node = get_node(list, index-1);	
		removed_node = previous_node->next;
		
		//We don't want a pointer to an invalid area of memory once we deallocate
		previous_node->next = NULL;
		list->last = previous_node;

	}  else if((index > 0) && (index < list->size-1))
	{ //middle
		node_t *previous_node = get_node(list, index-1);	
		removed_node = previous_node->next;
		previous_node->next = previous_node->next->next;
	}
 
	free(removed_node);
	list->size--;
}	

int list_find (list_t *list, int item)
{
	node_t *node = list->first;
	int i;
	for(i=0; i<list->size; i++)
	{
		//if the node with the index i matches the value we are searching for, return the index.
		if(node->data == item)
		{		
			return i;
		} else
		{	
			node = node->next;
		}		
	}
	//not found
    	return -1;
}

int list_get (list_t *list, int index)
{		
	if((index < 0)||(index >= list->size))
	{	
		return -1;
	}

	node_t *node = get_node(list, index);
	
	if(node != NULL)
	{	
		return node->data;
	} else
	{
		return -1;
	}
}

node_t *get_node(list_t *list, int index)
{
	if(!index_is_valid(list, index))
	{
	return NULL;
	}

	node_t *node = list->first;
	int i = 0;
	//iterate through the indexes, if the node is found, we want to return the pointer.
	while((i != index) && (i < list->size))
	{
		i++;
		node = node->next;
	}

	return node;	
}

bool index_is_valid(list_t *list, int index)
{
	if((index < 0) || (index >= list->size)) return false;
	return true;
}

